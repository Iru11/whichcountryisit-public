package com.example.davidiruela.wichcountryisit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

public class MenuScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_screen);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public void onClickAmSur(View view){
        Intent intent = new Intent(this, AmericaSur.class);
        startActivity(intent);
    }

    public void onClickAmNor(View view){

    }

    public void onClickAmCent(View view){

    }

    public void onClickEur(View view){

    }

    public void onClickAfr(View view){

    }

    public void onClickAsia(View view){

    }

    public void onClickOce(View view){

    }

    public void onClickAnt(View view){

    }

    public void onClickMon(View view){

    }

    public void onClickMostraPaises(View view){
        Intent intent = new Intent(this, HelpCountries.class);
        startActivity(intent);
    }
}
