package com.example.davidiruela.wichcountryisit;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class HelpCountries extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private GridView gridView;
    private AdaptadorAmericaSur adaptadorAmericaSur;
    private AdaptadorAmericaNorte adaptadorAmericaNorte;
    private AdaptadorAmericaCentral adaptadorAmericaCentral;
    private AdaptadorEuropa adaptadorEuropa;
    private AdaptadorAfrica adaptadorAfrica;
    private AdaptadorAsia adaptadorAsia;
    private AdaptadorOceania adaptadorOceania;
    private AdaptadorAntartida adaptadorAntartida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_countries);

        Toast.makeText(this, "Escojer continente para ayuda", Toast.LENGTH_LONG).show();
        gridView = (GridView) findViewById(R.id.grid);
        gridView.setOnItemClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.activity_amSur:
                adaptadorAmericaSur = new AdaptadorAmericaSur(this);
                gridView.setAdapter(adaptadorAmericaSur);
                return true;

            case R.id.activity_amNor:
                adaptadorAmericaNorte = new AdaptadorAmericaNorte(this);
                gridView.setAdapter(adaptadorAmericaNorte);
                return true;

            case R.id.activity_amCent:
                adaptadorAmericaCentral = new AdaptadorAmericaCentral(this);
                gridView.setAdapter(adaptadorAmericaCentral);
                return true;

            case R.id.activity_eur:
                adaptadorEuropa = new AdaptadorEuropa(this);
                gridView.setAdapter(adaptadorEuropa);
                return true;

            case R.id.activity_asia:
                adaptadorAsia = new AdaptadorAsia(this);
                gridView.setAdapter(adaptadorAsia);
                return true;

            case R.id.activity_africa:
                adaptadorAfrica = new AdaptadorAfrica(this);
                gridView.setAdapter(adaptadorAfrica);
                return true;

            case R.id.activity_oce:
                adaptadorOceania = new AdaptadorOceania(this);
                gridView.setAdapter(adaptadorOceania);
                return true;

            case R.id.activity_ant:
                adaptadorAntartida = new AdaptadorAntartida(this);
                gridView.setAdapter(adaptadorAntartida);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Pais item = (Pais) parent.getItemAtPosition(position);

        Intent intent = new Intent(this, Detealle.class);
        intent.putExtra(Detealle.EXTRA_PARAM_ID, item.getId());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            ActivityOptionsCompat activityOptions =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                            this,
                            new Pair<View, String>(view.findViewById(R.id.imagen_pais),
                                    Detealle.VIEW_NAME_HEADER_IMAGE)
                    );

            ActivityCompat.startActivity(this, intent, activityOptions.toBundle());
        } else
            startActivity(intent);
    }

}
