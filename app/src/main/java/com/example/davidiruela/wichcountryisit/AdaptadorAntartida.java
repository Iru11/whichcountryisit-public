package com.example.davidiruela.wichcountryisit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by davidiruela on 20/12/16.
 */

public class AdaptadorAntartida extends BaseAdapter{
    private Context context;

    public AdaptadorAntartida(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return Pais.ITEMSANTARTIDA.length;
    }

    @Override
    public Pais getItem(int position) {
        return Pais.ITEMSANTARTIDA[position];
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);
        }


        ImageView imagenCoche = (ImageView) view.findViewById(R.id.imagen_pais);
        TextView nombreCoche = (TextView) view.findViewById(R.id.name_country);


        final Pais item = getItem(position);
        Glide.with(imagenCoche.getContext())
                .load(item.getIdDrawable())
                .into(imagenCoche);

        nombreCoche.setText(item.getName());

        return view;
    }
}
