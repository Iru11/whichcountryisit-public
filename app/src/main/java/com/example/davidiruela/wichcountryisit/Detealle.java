package com.example.davidiruela.wichcountryisit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Detealle extends AppCompatActivity {

    public static final String EXTRA_PARAM_ID = "com.herprogramacion.coches2015.extra.ID";
    public static final String VIEW_NAME_HEADER_IMAGE = "imagen_compartida";
    private Pais itemDetallado;
    private ImageView imagenExtendida;
    private TextView nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detealle);

        // Obtener el Pais con el identificador establecido en la actividad principal
        itemDetallado = Pais.getItemMundo(getIntent().getIntExtra(EXTRA_PARAM_ID, 0));


        imagenExtendida = (ImageView) findViewById(R.id.imagen_extendida);
        nom = (TextView) findViewById(R.id.nombrePaisDetalle);
        nom.setText(itemDetallado.getName().toString());
        cargarImagenExtendida();
    }

    private void cargarImagenExtendida() {

        Glide.with(imagenExtendida.getContext())
                .load(itemDetallado.getIdDrawable())
                .into(imagenExtendida);
    }

}
