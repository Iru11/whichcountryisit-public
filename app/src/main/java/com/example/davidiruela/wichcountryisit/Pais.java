package com.example.davidiruela.wichcountryisit;

/**
 * Created by davidiruela on 15/12/16.
 */

public class Pais {
    private String name;
    private int idDrawable;

    public Pais (String name, int idDrawable){
        this.name = name;
        this.idDrawable = idDrawable;
    }

    public String getName() {
        return name;
    }

    public int getIdDrawable() {
        return idDrawable;
    }

    public int getId() {
        return name.hashCode();
    }

    public static Pais[] ITEMSAMERICASUR = {
            new Pais("ARGENTINA", R.drawable.argentina),
            new Pais("BOLIVIA", R.drawable.bolivia),
            new Pais("BRASIL", R.drawable.brasil),
            new Pais("CHILE", R.drawable.chile),
            new Pais("COLOMBIA", R.drawable.colombia),
            new Pais("ECUADOR", R.drawable.ecuador),
            new Pais("GUAYANA", R.drawable.guayana),
            new Pais("GUAYANA FRANCESA", R.drawable.guayanafrancesa),
    };

    public static Pais[] ITEMSAMERICANORTE = {
            new Pais("CANADA", R.drawable.canada),
            new Pais("ESTADOS UNIDOS", R.drawable.esadosunidos),
            new Pais("MEXICO", R.drawable.mexico),
    };

    public static Pais[] ITEMSAMERICACENTRAL = {
            new Pais("ANGUILA", R.drawable.anguila),
            new Pais("ANTIGUA Y BARBUDA", R.drawable.antiguaybarbuda),
            new Pais("BAHAMAS", R.drawable.bahamas),
            new Pais("BARBADOS", R.drawable.barbados),
            new Pais("BELICE", R.drawable.belice),
            new Pais("BERMUDA", R.drawable.bermuda),
            new Pais("COSTA RICA", R.drawable.costarica),
            new Pais("CUBA", R.drawable.cuba),
            new Pais("DOMINICA", R.drawable.dominica),
            new Pais("EL SALVADOR", R.drawable.elsalvador),
            new Pais("GRENADA", R.drawable.grenada),
            new Pais("GUATEMALA", R.drawable.guatemala),
            new Pais("HAITI", R.drawable.haiti),
            new Pais("HONDURAS", R.drawable.honduras),
            new Pais("JAMAICA", R.drawable.jamaica),
            new Pais("NICARAGUA", R.drawable.nicaragua),
            new Pais("PANAMA", R.drawable.panama),
            new Pais("PUERTO RICO", R.drawable.puertorico),
            new Pais("REPUBLICA DOMINICANA", R.drawable.republicadominicana),
            new Pais("SANTA LUCIA", R.drawable.santalucia),
            new Pais("TRINIDAD Y TOBAGO", R.drawable.trinidadytobago),
    };

    public static Pais[] ITEMSEUROPA = {
            new Pais("ALBANIA", R.drawable.albania),
            new Pais("ALEMANIA", R.drawable.alemania),
            new Pais("ANDORRA", R.drawable.andorra),
            new Pais("AUSTRIA", R.drawable.austria),
            new Pais("BELGICA", R.drawable.belgica),
            new Pais("BOSNIA", R.drawable.bosnia),
            new Pais("BULGARIA", R.drawable.bulgaria),
            new Pais("CHIPRE", R.drawable.chipre),
            new Pais("CIUDAD DEL VATICANO", R.drawable.ciudaddelvaticano),
            new Pais("CROACIA", R.drawable.croacia),
            new Pais("DINAMARCA", R.drawable.dinamarca),
            new Pais("ESCOCIA", R.drawable.escocia),
            new Pais("ESLOVAQUIA", R.drawable.eslovaquia),
            new Pais("ESLOVENIA", R.drawable.eslovenia),
            new Pais("ESPAÑA", R.drawable.espana),
            new Pais("ESTONIA", R.drawable.estonia),
            new Pais("FINLANDIA", R.drawable.finlandia),
            new Pais("FRANCIA", R.drawable.francia),
            new Pais("GALES", R.drawable.gales),
            new Pais("GIBRALTAR", R.drawable.gibraltar),
            new Pais("GRECIA", R.drawable.grecia),
            new Pais("HUNGRIA", R.drawable.hungria),
            new Pais("INGLATERRA", R.drawable.inglaterra),
            new Pais("IRLANDA", R.drawable.irlanda),
            new Pais("ISLANDIA", R.drawable.islandia),
            new Pais("ITALIA", R.drawable.italia),
            new Pais("LETONIA", R.drawable.letonia),
            new Pais("LIECHTENSTEIN", R.drawable.liechtenstein),
            new Pais("LITUANIA", R.drawable.lituania),
            new Pais("LUXEMBURGO", R.drawable.luxemburgo),
            new Pais("MACEDONIA", R.drawable.macedonia),
            new Pais("MALTA", R.drawable.malta),
            new Pais("MONACO", R.drawable.monaco),
            new Pais("MONTENEGRO", R.drawable.montenegro),
            new Pais("NORUEGA", R.drawable.noruega),
            new Pais("PAISES BAJOS", R.drawable.paisesbajos),
            new Pais("POLONIA", R.drawable.polonia),
            new Pais("PORTUGAL", R.drawable.portugal),
            new Pais("REPUBLICA CHECA", R.drawable.republicacheca),
            new Pais("RUMANIA", R.drawable.rumania),
            new Pais("RUSIA", R.drawable.rusia),
            new Pais("SAN MARINO", R.drawable.sanmarino),
            new Pais("SERBIA", R.drawable.serbia),
            new Pais("SUECIA", R.drawable.suecia),
            new Pais("SUIZA", R.drawable.suiza),
            new Pais("UCRANIA", R.drawable.ucrania),
    };

    public static Pais[] ITEMSASIA = {
            new Pais("AFGANISTAN", R.drawable.afganistan),
            new Pais("ARABIA SAUDITA", R.drawable.arabiasaudita),
            new Pais("ARMENIA", R.drawable.armenia),
            new Pais("AZERBAYAN", R.drawable.azerbayan),
            new Pais("BAHREIN", R.drawable.bahrein),
            new Pais("BANGLADESH", R.drawable.bangladesh),
            new Pais("BHUTAN", R.drawable.bhutan),
            new Pais("BRUNEI", R.drawable.brunei),
            new Pais("CAMBOYA", R.drawable.camboya),
            new Pais("CHINA", R.drawable.china),
            new Pais("COREA DEL NORTE", R.drawable.coreanorte),
            new Pais("COREA DEL SUR", R.drawable.coreasur),
            new Pais("FILIPINAS", R.drawable.filipinas),
            new Pais("GEORGIA", R.drawable.georgia),
            new Pais("HONG KONG", R.drawable.hongkong),
            new Pais("INDONESIA", R.drawable.indonesia),
            new Pais("INDONIA", R.drawable.indonia),
            new Pais("IRAK", R.drawable.irak),
            new Pais("IRAN", R.drawable.iran),
            new Pais("ISRAEL", R.drawable.israel),
            new Pais("JAPON", R.drawable.japon),
            new Pais("JORDANIA", R.drawable.jordania),
            new Pais("KAZAJSTAN", R.drawable.kazajstan),
            new Pais("KIRGUISTAN", R.drawable.kirguistan),
            new Pais("KUWUAIT", R.drawable.kuwuait),
            new Pais("LAOS", R.drawable.laos),
            new Pais("LIBANO", R.drawable.libano),
            new Pais("MACAO", R.drawable.macao),
            new Pais("MALASIA", R.drawable.malasia),
            new Pais("MONGOLIA", R.drawable.mongolia),
            new Pais("MYANMAR", R.drawable.myanmar),
            new Pais("NEPAL", R.drawable.nepal),
            new Pais("OMAN", R.drawable.oman),
            new Pais("PAKISTAN", R.drawable.pakistan),
            new Pais("PALESTINA", R.drawable.palestina),
            new Pais("QATAR", R.drawable.qatar),
            new Pais("SINGAPUR", R.drawable.singapur),
            new Pais("SIRIA", R.drawable.siria),
            new Pais("SIRI LANKA", R.drawable.sirilanka),
            new Pais("TAILANDIA", R.drawable.tailandia),
            new Pais("TAJIKISTÁN", R.drawable.tajikistan),
            new Pais("TURKMENISTAN", R.drawable.turkmenistan),
            new Pais("TURQUIA", R.drawable.turquia),
            new Pais("UZBEQUISTAN", R.drawable.uzbequistan),
            new Pais("VIETNAM", R.drawable.vietnam),
            new Pais("YEMEN", R.drawable.yemen),
    };

    public static Pais[] ITEMSAFRICA = {
            new Pais("ANGOLA", R.drawable.angola),
            new Pais("ARGELIA", R.drawable.argelia),
            new Pais("BENIN", R.drawable.benin),
            new Pais("BOTSWANA", R.drawable.botswana),
            new Pais("BURKINA FASO", R.drawable.burkinafaso),
            new Pais("BURUNDI", R.drawable.burundi),
            new Pais("CABO VERDE", R.drawable.caboverde),
            new Pais("CAMERUN", R.drawable.camerun),
            new Pais("CHAD", R.drawable.chad),
            new Pais("COMORAS", R.drawable.comoras),
            new Pais("CONGO", R.drawable.congo),
            new Pais("COSTA DE MARFIL", R.drawable.costademarfil),
            new Pais("EGIPTO", R.drawable.egipto),
            new Pais("ERITREA", R.drawable.eritrea),
            new Pais("ETIOPIA", R.drawable.etiopia),
            new Pais("GABON", R.drawable.gabon),
            new Pais("GAMBIA", R.drawable.gambia),
            new Pais("GHANA", R.drawable.ghana),
            new Pais("GUINEA", R.drawable.guinea),
            new Pais("GUINEA BISSAU", R.drawable.guineabissau),
            new Pais("GUINEA ECUATORIAL", R.drawable.guineaecuatorial),
            new Pais("KENYA", R.drawable.kenya),
            new Pais("LESOTHO", R.drawable.lesotho),
            new Pais("LIBERIA", R.drawable.liberia),
            new Pais("LIBIA", R.drawable.libia),
            new Pais("MADAGASCAR", R.drawable.madagascar),
            new Pais("MALAWI", R.drawable.malawi),
            new Pais("MALI", R.drawable.mali),
            new Pais("MARRUECOS", R.drawable.marruecos),
            new Pais("MAURICIO", R.drawable.mauricio),
            new Pais("MAURITANIA", R.drawable.mauritania),
            new Pais("MOZAMBIQUE", R.drawable.mozambique),
            new Pais("NAMIBA", R.drawable.namibia),
            new Pais("NIGER", R.drawable.niger),
            new Pais("NIGERIA", R.drawable.nigeria),
            new Pais("REPUBLICA CENTROAFRICANA", R.drawable.republicacentroafricana),
            new Pais("RUANDA", R.drawable.ruanda),
            new Pais("SAHARA OCCIDENTAL", R.drawable.saharaoccidental),
            new Pais("SANTA HELENA", R.drawable.santahelena),
            new Pais("SANTO TOMAS Y PRINCIPE", R.drawable.santotomasyprincipe),
            new Pais("SENEGAL", R.drawable.senegal),
            new Pais("SEYECHELLES", R.drawable.seychelles),
            new Pais("SIERRA LEONA", R.drawable.sierraleona),
            new Pais("SOMALIA", R.drawable.somalia),
            new Pais("SUDAFRICA", R.drawable.sudafrica),
            new Pais("SUDAN", R.drawable.sudan),
            new Pais("SWAZILANDIA", R.drawable.swazilandia),
            new Pais("TANZANIA", R.drawable.tanzania),
            new Pais("TOGO", R.drawable.togo),
            new Pais("TUNEZ", R.drawable.tunez),
            new Pais("UGANDA", R.drawable.uganda),
            new Pais("ZAMBIA", R.drawable.zambia),
            new Pais("ZIMBAWE", R.drawable.zimbabwe),
    };

    public static Pais[] ITEMSOCEANIA = {
            new Pais("AUSTRALIA", R.drawable.australia),
            new Pais("FIJI", R.drawable.fiji),
            new Pais("GUAM", R.drawable.guam),
            new Pais("ISLAS MARSHALL", R.drawable.islasmarshall),
            new Pais("ISLAS SALMON", R.drawable.islassalmon),
            new Pais("KIRIBATI", R.drawable.kiribati),
            new Pais("MICRONESIA", R.drawable.micronesia),
            new Pais("NAURU", R.drawable.nauru),
            new Pais("NUEVA ZELANDIA", R.drawable.nuevazelandia),
            new Pais("PALAU", R.drawable.palau),
            new Pais("PAPUA NUEVA GUINEA", R.drawable.papuanuevaguinea),
            new Pais("SAMOA", R.drawable.samoa),
            new Pais("TONGA", R.drawable.tonga),
            new Pais("TUVALU", R.drawable.tuvalu),
            new Pais("VANUATU", R.drawable.vanuatu),
    };

    public static Pais[] ITEMSANTARTIDA = {
            new Pais("ANTARTICA", R.drawable.antartica),
    };

    public static Pais[] ITEMSMUNDO = {
            //america sur
            new Pais("ARGENTINA", R.drawable.argentina),
            new Pais("BOLIVIA", R.drawable.bolivia),
            new Pais("BRASIL", R.drawable.brasil),
            new Pais("CHILE", R.drawable.chile),
            new Pais("COLOMBIA", R.drawable.colombia),
            new Pais("ECUADOR", R.drawable.ecuador),
            new Pais("GUAYANA", R.drawable.guayana),
            new Pais("GUAYANA FRANCESA", R.drawable.guayanafrancesa),
            //america norte
            new Pais("CANADA", R.drawable.canada),
            new Pais("ESTADOS UNIDOS", R.drawable.esadosunidos),
            new Pais("MEXICO", R.drawable.mexico),
            //america central
            new Pais("ANGUILA", R.drawable.anguila),
            new Pais("ANTIGUA Y BARBUDA", R.drawable.antiguaybarbuda),
            new Pais("BAHAMAS", R.drawable.bahamas),
            new Pais("BARBADOS", R.drawable.barbados),
            new Pais("BELICE", R.drawable.belice),
            new Pais("BERMUDA", R.drawable.bermuda),
            new Pais("COSTA RICA", R.drawable.costarica),
            new Pais("CUBA", R.drawable.cuba),
            new Pais("DOMINICA", R.drawable.dominica),
            new Pais("EL SALVADOR", R.drawable.elsalvador),
            new Pais("GRENADA", R.drawable.grenada),
            new Pais("GUATEMALA", R.drawable.guatemala),
            new Pais("HAITI", R.drawable.haiti),
            new Pais("HONDURAS", R.drawable.honduras),
            new Pais("JAMAICA", R.drawable.jamaica),
            new Pais("NICARAGUA", R.drawable.nicaragua),
            new Pais("PANAMA", R.drawable.panama),
            new Pais("PUERTO RICO", R.drawable.puertorico),
            new Pais("REPUBLICA DOMINICANA", R.drawable.republicadominicana),
            new Pais("SANTA LUCIA", R.drawable.santalucia),
            new Pais("TRINIDAD Y TOBAGO", R.drawable.trinidadytobago),
            //europa
            new Pais("ALBANIA", R.drawable.albania),
            new Pais("ALEMANIA", R.drawable.alemania),
            new Pais("ANDORRA", R.drawable.andorra),
            new Pais("AUSTRIA", R.drawable.austria),
            new Pais("BELGICA", R.drawable.belgica),
            new Pais("BOSNIA", R.drawable.bosnia),
            new Pais("BULGARIA", R.drawable.bulgaria),
            new Pais("CHIPRE", R.drawable.chipre),
            new Pais("CIUDAD DEL VATICANO", R.drawable.ciudaddelvaticano),
            new Pais("CROACIA", R.drawable.croacia),
            new Pais("DINAMARCA", R.drawable.dinamarca),
            new Pais("ESCOCIA", R.drawable.escocia),
            new Pais("ESLOVAQUIA", R.drawable.eslovaquia),
            new Pais("ESLOVENIA", R.drawable.eslovenia),
            new Pais("ESPAÑA", R.drawable.espana),
            new Pais("ESTONIA", R.drawable.estonia),
            new Pais("FINLANDIA", R.drawable.finlandia),
            new Pais("FRANCIA", R.drawable.francia),
            new Pais("GALES", R.drawable.gales),
            new Pais("GIBRALTAR", R.drawable.gibraltar),
            new Pais("GRECIA", R.drawable.grecia),
            new Pais("HUNGRIA", R.drawable.hungria),
            new Pais("INGLATERRA", R.drawable.inglaterra),
            new Pais("IRLANDA", R.drawable.irlanda),
            new Pais("ISLANDIA", R.drawable.islandia),
            new Pais("ITALIA", R.drawable.italia),
            new Pais("LETONIA", R.drawable.letonia),
            new Pais("LIECHTENSTEIN", R.drawable.liechtenstein),
            new Pais("LITUANIA", R.drawable.lituania),
            new Pais("LUXEMBURGO", R.drawable.luxemburgo),
            new Pais("MACEDONIA", R.drawable.macedonia),
            new Pais("MALTA", R.drawable.malta),
            new Pais("MONACO", R.drawable.monaco),
            new Pais("MONTENEGRO", R.drawable.montenegro),
            new Pais("NORUEGA", R.drawable.noruega),
            new Pais("PAISES BAJOS", R.drawable.paisesbajos),
            new Pais("POLONIA", R.drawable.polonia),
            new Pais("PORTUGAL", R.drawable.portugal),
            new Pais("REPUBLICA CHECA", R.drawable.republicacheca),
            new Pais("RUMANIA", R.drawable.rumania),
            new Pais("RUSIA", R.drawable.rusia),
            new Pais("SAN MARINO", R.drawable.sanmarino),
            new Pais("SERBIA", R.drawable.serbia),
            new Pais("SUECIA", R.drawable.suecia),
            new Pais("SUIZA", R.drawable.suiza),
            new Pais("UCRANIA", R.drawable.ucrania),
            //asia
            new Pais("AFGANISTAN", R.drawable.afganistan),
            new Pais("ARABIA SAUDITA", R.drawable.arabiasaudita),
            new Pais("ARMENIA", R.drawable.armenia),
            new Pais("AZERBAYAN", R.drawable.azerbayan),
            new Pais("BAHREIN", R.drawable.bahrein),
            new Pais("BANGLADESH", R.drawable.bangladesh),
            new Pais("BHUTAN", R.drawable.bhutan),
            new Pais("BRUNEI", R.drawable.brunei),
            new Pais("CAMBOYA", R.drawable.camboya),
            new Pais("CHINA", R.drawable.china),
            new Pais("COREA DEL NORTE", R.drawable.coreanorte),
            new Pais("COREA DEL SUR", R.drawable.coreasur),
            new Pais("FILIPINAS", R.drawable.filipinas),
            new Pais("GEORGIA", R.drawable.georgia),
            new Pais("HONG KONG", R.drawable.hongkong),
            new Pais("INDONESIA", R.drawable.indonesia),
            new Pais("INDONIA", R.drawable.indonia),
            new Pais("IRAK", R.drawable.irak),
            new Pais("IRAN", R.drawable.iran),
            new Pais("ISRAEL", R.drawable.israel),
            new Pais("JAPON", R.drawable.japon),
            new Pais("JORDANIA", R.drawable.jordania),
            new Pais("KAZAJSTAN", R.drawable.kazajstan),
            new Pais("KIRGUISTAN", R.drawable.kirguistan),
            new Pais("KUWUAIT", R.drawable.kuwuait),
            new Pais("LAOS", R.drawable.laos),
            new Pais("LIBANO", R.drawable.libano),
            new Pais("MACAO", R.drawable.macao),
            new Pais("MALASIA", R.drawable.malasia),
            new Pais("MONGOLIA", R.drawable.mongolia),
            new Pais("MYANMAR", R.drawable.myanmar),
            new Pais("NEPAL", R.drawable.nepal),
            new Pais("OMAN", R.drawable.oman),
            new Pais("PAKISTAN", R.drawable.pakistan),
            new Pais("PALESTINA", R.drawable.palestina),
            new Pais("QATAR", R.drawable.qatar),
            new Pais("SINGAPUR", R.drawable.singapur),
            new Pais("SIRIA", R.drawable.siria),
            new Pais("SIRI LANKA", R.drawable.sirilanka),
            new Pais("TAILANDIA", R.drawable.tailandia),
            new Pais("TAJIKISTÁN", R.drawable.tajikistan),
            new Pais("TURKMENISTAN", R.drawable.turkmenistan),
            new Pais("TURQUIA", R.drawable.turquia),
            new Pais("UZBEQUISTAN", R.drawable.uzbequistan),
            new Pais("VIETNAM", R.drawable.vietnam),
            new Pais("YEMEN", R.drawable.yemen),
            //africa
            new Pais("ANGOLA", R.drawable.angola),
            new Pais("ARGELIA", R.drawable.argelia),
            new Pais("BENIN", R.drawable.benin),
            new Pais("BOTSWANA", R.drawable.botswana),
            new Pais("BURKINA FASO", R.drawable.burkinafaso),
            new Pais("BURUNDI", R.drawable.burundi),
            new Pais("CABO VERDE", R.drawable.caboverde),
            new Pais("CAMERUN", R.drawable.camerun),
            new Pais("CHAD", R.drawable.chad),
            new Pais("COMORAS", R.drawable.comoras),
            new Pais("CONGO", R.drawable.congo),
            new Pais("COSTA DE MARFIL", R.drawable.costademarfil),
            new Pais("EGIPTO", R.drawable.egipto),
            new Pais("ERITREA", R.drawable.eritrea),
            new Pais("ETIOPIA", R.drawable.etiopia),
            new Pais("GABON", R.drawable.gabon),
            new Pais("GAMBIA", R.drawable.gambia),
            new Pais("GHANA", R.drawable.ghana),
            new Pais("GUINEA", R.drawable.guinea),
            new Pais("GUINEA BISSAU", R.drawable.guineabissau),
            new Pais("GUINEA ECUATORIAL", R.drawable.guineaecuatorial),
            new Pais("KENYA", R.drawable.kenya),
            new Pais("LESOTHO", R.drawable.lesotho),
            new Pais("LIBERIA", R.drawable.liberia),
            new Pais("LIBIA", R.drawable.libia),
            new Pais("MADAGASCAR", R.drawable.madagascar),
            new Pais("MALAWI", R.drawable.malawi),
            new Pais("MALI", R.drawable.mali),
            new Pais("MARRUECOS", R.drawable.marruecos),
            new Pais("MAURICIO", R.drawable.mauricio),
            new Pais("MAURITANIA", R.drawable.mauritania),
            new Pais("MOZAMBIQUE", R.drawable.mozambique),
            new Pais("NAMIBA", R.drawable.namibia),
            new Pais("NIGER", R.drawable.niger),
            new Pais("NIGERIA", R.drawable.nigeria),
            new Pais("REPUBLICA CENTROAFRICANA", R.drawable.republicacentroafricana),
            new Pais("RUANDA", R.drawable.ruanda),
            new Pais("SAHARA OCCIDENTAL", R.drawable.saharaoccidental),
            new Pais("SANTA HELENA", R.drawable.santahelena),
            new Pais("SANTO TOMAS Y PRINCIPE", R.drawable.santotomasyprincipe),
            new Pais("SENEGAL", R.drawable.senegal),
            new Pais("SEYECHELLES", R.drawable.seychelles),
            new Pais("SIERRA LEONA", R.drawable.sierraleona),
            new Pais("SOMALIA", R.drawable.somalia),
            new Pais("SUDAFRICA", R.drawable.sudafrica),
            new Pais("SUDAN", R.drawable.sudan),
            new Pais("SWAZILANDIA", R.drawable.swazilandia),
            new Pais("TANZANIA", R.drawable.tanzania),
            new Pais("TOGO", R.drawable.togo),
            new Pais("TUNEZ", R.drawable.tunez),
            new Pais("UGANDA", R.drawable.uganda),
            new Pais("ZAMBIA", R.drawable.zambia),
            new Pais("ZIMBAWE", R.drawable.zimbabwe),
            //oceania
            new Pais("AUSTRALIA", R.drawable.australia),
            new Pais("FIJI", R.drawable.fiji),
            new Pais("GUAM", R.drawable.guam),
            new Pais("ISLAS MARSHALL", R.drawable.islasmarshall),
            new Pais("ISLAS SALMON", R.drawable.islassalmon),
            new Pais("KIRIBATI", R.drawable.kiribati),
            new Pais("MICRONESIA", R.drawable.micronesia),
            new Pais("NAURU", R.drawable.nauru),
            new Pais("NUEVA ZELANDIA", R.drawable.nuevazelandia),
            new Pais("PALAU", R.drawable.palau),
            new Pais("PAPUA NUEVA GUINEA", R.drawable.papuanuevaguinea),
            new Pais("SAMOA", R.drawable.samoa),
            new Pais("TONGA", R.drawable.tonga),
            new Pais("TUVALU", R.drawable.tuvalu),
            new Pais("VANUATU", R.drawable.vanuatu),
            //antartida
            new Pais("ANTARTICA", R.drawable.antartica),
    };

    /**
     * Obtiene item basado en su identificador
     *
     * @param id identificador
     * @return Pais
     */
    public static Pais getItemAmericaSur(int id) {
        for (Pais item : ITEMSAMERICASUR) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemAmericaNorte(int id) {
        for (Pais item : ITEMSAMERICANORTE) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemAmericaCentral(int id) {
        for (Pais item : ITEMSAMERICACENTRAL) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemEuropa(int id) {
        for (Pais item : ITEMSEUROPA) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemAsia(int id) {
        for (Pais item : ITEMSASIA) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemAfrica(int id) {
        for (Pais item : ITEMSAFRICA) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemOceania(int id) {
        for (Pais item : ITEMSOCEANIA) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemAntartida(int id) {
        for (Pais item : ITEMSANTARTIDA) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
    public static Pais getItemMundo(int id) {
        for (Pais item : ITEMSMUNDO) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
}
