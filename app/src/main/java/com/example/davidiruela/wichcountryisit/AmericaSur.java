package com.example.davidiruela.wichcountryisit;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

public class AmericaSur extends AppCompatActivity {

    ImageView ivAS;
    EditText etNomPais;
    private static final Random random = new Random();
    int i= 0;

    private static Integer[] imagenesID =
            {R.drawable.argentina, R.drawable.bolivia,};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_america_sur);
        ivAS = (ImageView) findViewById(R.id.ivAS);
        etNomPais = (EditText) findViewById(R.id.etNomPais);
    }

    public void onClickValida(View view){
        new ChangeImage().execute();
    }

    private class ChangeImage extends AsyncTask<Void, Void, Bitmap> {

        Bitmap bm;
        protected Bitmap doInBackground(Void... Params){
            Resources res = getResources();


           // for (int i = 0; i < imagenesID.length; i++) {
             //   id = Pais.getItemAmericaSur(imagenesID[i]);
             //   Log.d("AQUII: ", id.getIdDrawable()+"   "+id.getName()+"    "+id.getId());
           // }

            while(i < imagenesID.length) {
                int id =imagenesID[i];
                bm = BitmapFactory.decodeResource(res, id);

                Pais paisId = new Pais("", id);

                Log.d("PAIS: ", "" + paisId.getIdDrawable() + "   "+id);
            i++;
            }
            return bm;
        }
        protected void onPostExecute(Bitmap bm){


            ivAS.setImageBitmap(bm);



        }


    }
}
